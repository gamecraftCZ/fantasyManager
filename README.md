# fantasyManager
Manager for fantasy worlds.  
Just download [FantasyManager.jar](https://gitlab.com/gamecraftCZ/fantasyManager/blob/master/fantasyManager.jar) and run it.  
This software works only with java 8. Support for more versions will be soon.  
  

This software is under GNU-GPL license.  


Please donate me a coffee to support future development:  

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7PZ4ZRLFTXR32) (USD)

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZJJK5NN5F78L8) (CZK)

Bitcoin: If you want to danate in bitcoins, write me a mail: [gamecraftCZZ@gmail.com](mailto:gamecraftCZZ@gmail.com)    


  

Icon "edit" made by Freepik from www.flaticon.com  
Icon "delete" made by Egor Rumyantsev from www.flaticon.com 
